﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Session;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Kronos.Web.Models;
using Kronos.Web.Database;

namespace Kronos.Web.Controllers
{
    public class UserController : Controller
    {
        private readonly ILogger<UserController> _logger;
        private readonly IDB _DB;

        public UserController(ILogger<UserController> logger, IDB db)
        {
            _logger = logger;
            _DB = db;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View(new UserLoginModel());
        }

        [HttpPost]
        public IActionResult Login(UserLoginModel login)
        {
            if (!ModelState.IsValid)
            {
                return View(login);
            }
            var user = _DB.ValidateLogin(login.Username, login.Password);

            if (user != null)
            {
                HttpContext.Session.SetInt32("UserId", user.Id);
                HttpContext.Session.SetString("Username", user.Username);
                HttpContext.Session.SetInt32("UserType", (int)user.Type);
                return Redirect("/Expense/MyExpenses");
            }
            ModelState.AddModelError(string.Empty, "Invalid username or password");
            return View(login);
        }

    }
}
