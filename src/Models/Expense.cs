using System;
using System.ComponentModel.DataAnnotations;
using Kronos.Web.Database;

namespace Kronos.Web.Models
{

    public class AddExpenseModel
    {
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public decimal Amount { get; set; }
        [Required]
        public string InvoiceNumber { get; set; }
        [Required]
        public int CategoryId { get; set; }
        public string Description { get; set; }
    }

    public class ExpenseListItem
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string CategoryName { get; set; }
        public decimal Amount { get; set; }
    }

    public class UserLoginModel
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
    }

    public class UserInfo
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public UserType Type { get; set; }
    }

}
