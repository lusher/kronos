using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Kronos.Web.Models;

namespace Kronos.Web.Database {

	public enum UserType {
		Employee = 1,
		Manager = 2,
		Accountant = 3,
	}

	public class User {

		public int Id {get;set;}
		public string Username {get; set;}
		public string Password {get;set;}
		public UserType Type {get;set;}
	}

	public enum ExpenseApprovalStatus {
		Unapproved = 1,
		Approved = 2
	}

	public class Expense {

		public int Id {get;set;}
		public int UserId {get;set;}
		public DateTime Date {get;set;}
		public decimal Amount {get;set;}
		public string InvoiceNumber {get;set;}
		public string Description {get;set;}
		public int CategoryId {get;set;}
		public ExpenseApprovalStatus ApprovalStatus {get;set;}
		public string RejectedReason {get;set;}
	}

	public class ExpenseCategory {
		public int Id {get;set;}
		public string Name {get;set;}
	}

	public interface IDB {
		UserInfo ValidateLogin(string username, string password);
		List<SelectListItem> ListCategories();
		List<ExpenseListItem> ListExpenses(int userId);
		void AddExpense(int userId, AddExpenseModel model);
	}

	public class DB : IDB {
		private static readonly List<ExpenseCategory> Categories = new List<ExpenseCategory>() {
			new ExpenseCategory { Id = 0, Name = "Other" },
			new ExpenseCategory { Id = 1, Name = "Food" }
		};

		private static readonly List<User> Users = new List<User>() {
			new User { Id = 1, Username = "emp1", Password = "emp1", Type = UserType.Employee },
			new User { Id = 2, Username = "man1", Password = "man1", Type = UserType.Manager },
			new User { Id = 3, Username = "acc1", Password = "acc1", Type = UserType.Accountant },
		};
		private static readonly List<Expense> Expenses = new List<Expense>() {
			new Expense { Id = 1, Date = new DateTime(2021, 10, 11),  UserId = 1, Amount = 45, Description = "something", CategoryId = 1 },
			new Expense { Id = 2,  Date = new DateTime(2021, 10, 10), UserId = 1, Amount = 32, Description = "something", CategoryId = 1 },
		};

		public UserInfo ValidateLogin(string username, string password) {
				return Users.Where(x=>x.Username == username && x.Password == password).Select(x=> new UserInfo { Id = x.Id, Username =x.Username, Type = x.Type} ).FirstOrDefault();
		}

		public List<SelectListItem> ListCategories() {
			return Categories.Select(x=> new SelectListItem{ Value = x.Id.ToString(), Text = x.Name }).ToList();
		}

		public List<ExpenseListItem> ListExpenses(int userId) {
			return Expenses.Where(x=>x.UserId == userId)
				.Select(x=> new ExpenseListItem {
					Description = x.Description, 
					Amount = x.Amount, 
					Date = x.Date,
					CategoryName = Categories.Where(y=> x.CategoryId == y.Id).FirstOrDefault().Name
				}).ToList();
		}

		public void AddExpense(int userId, AddExpenseModel model) {
			Expenses.Add(new Expense {
				Id = Expenses.Max(x=>x.Id) + 1,
				UserId = userId,
				Date = model.Date,
				Amount = model.Amount,
				Description = model.Description,
				CategoryId = model.CategoryId,
				ApprovalStatus = ExpenseApprovalStatus.Unapproved
			});
		}

	}

}

