
Case Scenario:
Kronos has decided to create a web application for employees to save and track work related expenses. An employee submits an expense, a manager can review it and approve it. Once it is approved, then the accountant can see it and mark it as paid.

The requirements:
    1. Users will be authenticated with username/password. There will be 3 types of users. Users/employee, manager, and accountant. 
    2. Entering a future date is not permitted.
    3. Once each expense is entered in the form, the total amount will be automatically calculated on the bottom of the page.
    4. Managers can access a form that has “the waiting to be approved” entries, click the details, an option to approve or not. If not being approved, a reason text will be entered by the manager.
    5. Accountants can only see the approved forms, enter the details, mark them as paid. Once it is paid, the system should notify the employee via email informing that a particular expense is paid.
    6. If an expense form has been waiting more than 48 hours to be approved by a manager, the system should send a notification email to the manager regarding the “waiting to be approved” expenses.

The minimum required pages and fields:
Users:
    • Login
        ◦ Username/password
    • Expense list
        ◦ Datetime, amount, category, description
    • Enter expense form (can enter multiple in one)
        ◦ Expense datetime, Invoice No, Expense Category, Other Expense Category(if “Others” selected in Expense Category), Description, Amount
        ◦ Total Amount
    • Expense details
        ◦ Expense datetime, Invoice No, Expense Category, Other Expense Category(if “Others” selected in Expense Category), Description, Amount
        ◦ Total Amount
        ◦ Status – Reason not to be approved

Managers:
    • Login
        ◦ Username/password
    • Expense list
        ◦ Employee name, datetime, amount, category, description
    • Expense details
        ◦ Employee name, Expense datetime, Invoice No, Expense Category, Description, Amount/Total Amount
        ◦ Approve – Reject Buttons
            ▪ Reject Reason
Accountant:
    • Login
        ◦ Username/password
    • Expense list
        ◦ Employee name, Manager name, datetime, amount, category, description
    • Expense details
        ◦ Employee name, Expense datetime, Invoice No, Expense Category, Description, Amount/Total Amount
        ◦ Pay Button

Assumptions/considerations
    • You may use a mocking framework for emailing.
    • Technologies may be considered in the solution:
        ◦ Modern JavaScripts and Styling frameworks
        ◦ ASP.Net Core/MVC/Web API
        ◦ C# and .Net technologies
        ◦ MS Sql Server or NoSql 
        ◦ Azure App Services/Functions/Logic Apps
Demonstrate following skills 
    • Object oriented programming principles 
    • Design patterns
    • Scalable architecture 
    • Test driven development
Assessment Outcomes:
    • Single page document describing assumptions and a high-level solution diagram
    • Full Visual Studio solution and any steps required to run the solution within VS

