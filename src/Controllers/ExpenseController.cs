﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Session;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Kronos.Web.Models;
using Kronos.Web.Database;

namespace Kronos.Web.Controllers
{
    public class ExpenseController : Controller
    {
        private readonly ILogger<ExpenseController> _logger;
        private readonly IDB _DB;

        public ExpenseController(ILogger<ExpenseController> logger, IDB db)
        {
            _logger = logger;
						_DB = db;
        }

				[HttpGet]
        public IActionResult MyExpenses() {
					//TODO: use authentication attributes
					int? userId = HttpContext.Session.GetInt32("UserId");
					if (!userId.HasValue)
						return Redirect("/User/Login");
					var expenses = _DB.ListExpenses(userId.Value);
					return View(expenses);
				}

				[HttpGet]
        public IActionResult Add() {
					//TODO: use authentication attributes
					int? userId = HttpContext.Session.GetInt32("UserId");
					if (!userId.HasValue)
						return Redirect("/User/Login");

					ViewData["Categories"] = _DB.ListCategories();
					return View(new AddExpenseModel{ Date = DateTime.Today });
				}

				[HttpPost]
        public IActionResult Add(AddExpenseModel expense) {
					//TODO: use authentication attributes
					int? userId = HttpContext.Session.GetInt32("UserId");
					if (!userId.HasValue || userId.Value == 0)
						return Redirect("/User/Login");

					if (expense.CategoryId == 0 && string.IsNullOrWhiteSpace(expense.Description))
						ModelState.AddModelError(nameof(expense.Description), "must have description if category is other");
					if (expense.Amount < 1)
						ModelState.AddModelError(nameof(expense.Amount), "must be at least $1");
					if (expense.Date > DateTime.Today)
						ModelState.AddModelError(nameof(expense.Date), "no time travelling");

					if (ModelState.IsValid) {
						_DB.AddExpense(userId.Value, expense);
						return Redirect("/Expense/MyExpenses");
					}

					ViewData["Categories"] = _DB.ListCategories();
					return View(expense);
				}
				
    }
}
